# Changelog
All notable changes to this project will be documented in this file.

## [1.1.4] - 16th september 2020
### Added

* can handle 2 different types of input format CLASSIC and RAPIDRUN
* tutorial to process data in CLASSIC format based on a subset of Rhone project data

### Changed

* rapidrun all_samples.tsv replaced by all_samples.csv with `;` as separator

### Removed

* Unuseful or deprecated config files


## [1.1.3] - 27th july 2020

### Added

* tutorial commands and folders
* tutorial configuration file
* clean.sh a script to remove intermediate files, log files and final results files

### Changed

* main.sh launches whole workflow
* fix tutorial commands


## [1.1.2] - 21st july 2020

### Added

* multithreaded assembly step for large fastq files

### Changed

* fix main.sh so it can run into a single command

### Removed

* picture of DAG files


## [1.1.1] - 3rd july 2020

### Added

* Tutorial
* Documentation
* gitignore
* Autorship
* license MIT
* Singularity/Docker recipe for obitools

## [1.1.0] - 22th june 2020

### Added

* data test
* Conda v4.8.2 envs

### Changed

* upgrade snakemake to v5.19.2
* new structure for distribution based on snakemake recommandation [here](https://snakemake.readthedocs.io/en/v5.19.2/snakefiles/deployment.html)

### Removed

* infos folder


## [1.0.1] - 14th june 2020

### Changed

* Absolute path of all_samples.tsv into config.yaml
* Remove redundant paramters into config.yaml
* Fix name of count parameters into filter_samples.smk


## [1.0.0] - 25th may 2020

### Added
 
* Complete workflow
* snakemake rule system to handle rapidrun files

### Changed

* upgrade singularity to v3.6

### Removed

* marker.csv 
