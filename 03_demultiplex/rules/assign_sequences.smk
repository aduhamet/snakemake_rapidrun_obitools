__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Assign each sequence record to the corresponding sample/marker combination
rule assign_sequences:
    output:
        assign='../results/03_demultiplex/01_assign_sequences/{demultiplex}.ali.assigned.fastq',
        unid='../results/03_demultiplex/01_assign_sequences/{demultiplex}.unidentified.fastq'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:        
        dmulti= lambda wildcards: dfRunMarker[dfRunMarker.projMarkRun == wildcards.demultiplex].to_dict('records')[0],
    log:
        '../logs/03_demultiplex/01_assign_sequences/{demultiplex}.log'
    shell:
        '''ngsfilter -t {params.dmulti[dat]} -u {output.unid} ../results/02_assembly/02_remove_unaligned/{params.dmulti[run]}.ali.fastq --fasta-output > {output.assign} 2> {log}'''

