__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Split the input sequence file in a set of subfiles according to the values of attribute `sample`
rule split_sequences:
    input:
        '../results/03_demultiplex/01_assign_sequences/{demultiplex}.ali.assigned.fastq'
    params:
        dir='../results/03_demultiplex/02_raw/{demultiplex}/'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        '../logs/03_demultiplex/02_split_sequences/{demultiplex}.log'
    shell:
        '''mkdir -p {params.dir}; obisplit -p "{params.dir}" -t sample --fasta {input} 2> {log}'''
