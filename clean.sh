MODE=$1

if [ -d "results" ]; then  
  if [ "$MODE" == "clear" ]; then
    echo -n "Clearing..."
    mv -i results cleared/
    mkdir results
    mkdir results/01_settings results/02_assembly results/03_demultiplex results/04_filter_samples results/05_assignment results/06_final_tables
    mkdir results/02_assembly/01_illuminapairedend  results/02_assembly/02_remove_unaligned
    mkdir results/03_demultiplex/00_dat  results/03_demultiplex/01_assign_sequences  results/03_demultiplex/02_raw
    mkdir results/04_filter_samples/01_dereplicated  results/04_filter_samples/02_goodlength  results/04_filter_samples/03_clean_pcrerr  results/04_filter_samples/04_filtered
    mkdir results/05_assignment/01_runs  results/05_assignment/02_dereplicated  results/05_assignment/03_assigned  results/05_assignment/04_formated
    echo "done"
    echo "Previous result files are now stored into 'cleared' folder."
    echo "A new folder 'results' with empty subfolders have been created."
  elif [ "$MODE" == "reset" ]; then
    echo -n "Resetting..."
    rm -r results
    mkdir results
    mkdir results/01_settings results/02_assembly results/03_demultiplex results/04_filter_samples results/05_assignment results/06_final_tables
    mkdir results/02_assembly/01_illuminapairedend  results/02_assembly/02_remove_unaligned
    mkdir results/03_demultiplex/00_dat  results/03_demultiplex/01_assign_sequences  results/03_demultiplex/02_raw
    mkdir results/04_filter_samples/01_dereplicated  results/04_filter_samples/02_goodlength  results/04_filter_samples/03_clean_pcrerr  results/04_filter_samples/04_filtered
    mkdir results/05_assignment/01_runs  results/05_assignment/02_dereplicated  results/05_assignment/03_assigned  results/05_assignment/04_formated
    echo "done"
    echo "Previous result files have been removed."
    echo "A new folder 'results' with empty subfolders have been created."
  else
    printf "clean.sh don't work because you didn't set an action to perform as first argument:\n$ bash clean.sh clear\n    move current result folder into a folder named 'cleared' and create again a new folder result\n$ bash clean.sh reset\n    remove current result folder and create a new folder results\n"
  fi
else
  echo "Cleaning is impossible... you are not in the right folder."
fi