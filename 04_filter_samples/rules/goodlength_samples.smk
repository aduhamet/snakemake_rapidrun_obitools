__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## only sequence more than 20bp with no ambiguity IUAPC with total coverage greater than 10 reads
rule goodlength_samples:
    input:
        '../results/04_filter_samples/01_dereplicated/{demultiplexs}.uniq.fasta'
    output:
        '../results/04_filter_samples/02_goodlength/{demultiplexs}.l.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        '../logs/04_filter_samples/02_goodlength/{demultiplexs}.log'
    params:
        seq_count=config["good_length_samples"]["seq_count"],
        seq_length_min=config["good_length_samples"]["seq_length_min"]
    shell:
        '''obigrep  -p 'count>{params.seq_count}' -s '^[ACGT]+$' -p 'seq_length>{params.seq_length_min}' {input} > {output} 2> {log}'''