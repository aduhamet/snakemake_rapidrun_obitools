__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"

## dereplicate reads into uniq sequences
rule dereplicate_samples:
    input:
        '../results/03_demultiplex/02_raw/{demultiplexs}.fasta'
    output:
        '../results/04_filter_samples/01_dereplicated/{demultiplexs}.uniq.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        '../logs/04_filter_samples/01_dereplicated/{demultiplexs}.log'
    params:
        dmulti= lambda wildcards: dfMultiChecked[dfMultiChecked.demultiplex == wildcards.demultiplexs].to_dict('records')[0],        
    shell:
        '''mkdir -p ../results/04_filter_samples/01_dereplicated/{params.dmulti[projmarkrun]}; obiuniq -m sample {input} > {output} 2> {log}'''