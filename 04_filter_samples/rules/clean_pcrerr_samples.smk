__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Clean the sequences for PCR/sequencing errors (sequence variants)
rule clean_pcrerr_samples:
    input:
        '../results/04_filter_samples/02_goodlength/{demultiplexs}.l.u.fasta'
    output:
        '../results/04_filter_samples/03_clean_pcrerr/{demultiplexs}.r.l.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        '../logs/04_filter_samples/03_clean_pcrerr/{demultiplexs}.log'
    params:
         r=config["clean_pcrerr_samples"]["r"]
    shell:
        '''if [[ -s {input} ]]; then obiclean -r {params.r} {input} > {output} 2> {log} ; else touch {output} 2> {log} ; fi'''