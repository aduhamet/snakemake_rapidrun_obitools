__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Remove sequence which are classified as 'internal' by obiclean
rule rm_internal_samples:
    input:
        '../results/04_filter_samples/03_clean_pcrerr/{demultiplexs}.r.l.u.fasta'
    output:
        '../results/04_filter_samples/04_filtered/{demultiplexs}.c.r.l.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    params:
        dmulti= lambda wildcards: dfMultiChecked[dfMultiChecked.demultiplex == wildcards.demultiplexs].to_dict('records')[0],
    singularity:
        config["singularity"]["obitools"]
    log:
        '../logs/04_filter_samples/04_filtered/{demultiplexs}.log'
    shell:
        '''if [[ -s {input} ]]; then mkdir -p ../results/04_filter_samples/04_filtered/{params.dmulti[projmarkrun]}; obigrep -p "obiclean_internalcount == 0" {input} > {output} 2> {log} ; else touch {output} 2> {log} ; fi'''
