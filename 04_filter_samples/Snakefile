#===============================================================================
#HEADER
#===============================================================================
__author__ = "Pierre-Edouard Guerin"
__credits__ = ["Pierre-Edouard Guerin", "Virginie Marques"]
__license__ = "MIT"
__version__ = "1.1.4"
__maintainer__ = "Pierre-Edouard Guerin"
__email__ = "pierre-edouard.guerin@cefe.cnrs.fr"
__status__ = "Production"
"""
Codes for scientific papers related to metabarcoding studies

AUTHORS
=======
* Pierre-Edouard Guerin   | pierre-edouard.guerin@cefe.cnrs.fr
* Virginie Marques        | virginie.marques@cefe.cnrs.fr
* CNRS/CEFE, CNRS/MARBEC  | Montpellier, France
* 2018-2020


DESCRIPTION
===========
This is a Snakefile using SNAKEMAKE workflow management system
From  config.yaml, demultiplexing.csv
From results/03_demultiplex/02_raw/`projet`/`marker`/`run`/`sample`.fasta
it will performs following rules:
1. dereplicate sequences at `sample` level
2. filters sequences with wrong length or low coverage or IUAPC ambiguity
3. detect PCR clones
4. remove PCR clones
"""

###############################################################################
# MODULES
###############################################################################
import pandas
import os.path


###############################################################################
# GLOBAL VARIABLES
###############################################################################

#configfile: "../config.yaml"

## load demultiplexing dataframe
dfMulti =pandas.read_csv("../results/01_settings/all_demultiplex.csv", sep=",")

## check demultiplexing results
dfMultiChecked = dfMulti
for thisDemultiplex in dfMulti.demultiplex:
    file_sample = "../results/03_demultiplex/02_raw/"+thisDemultiplex+".fasta"
    if not os.path.exists(file_sample):
        print("WARNING: ", file_sample," not found. We removed it from this analysis.")
        dfMultiChecked = dfMultiChecked[dfMultiChecked.demultiplex != thisDemultiplex]
print(dfMultiChecked)


###############################################################################
# RULES
###############################################################################

rule all:
    input:
        expand('../results/04_filter_samples/01_dereplicated/{demultiplexs}.uniq.fasta', demultiplexs=dfMultiChecked['demultiplex']),
        expand('../results/04_filter_samples/02_goodlength/{demultiplexs}.l.u.fasta', demultiplexs=dfMultiChecked['demultiplex']),
        expand('../results/04_filter_samples/03_clean_pcrerr/{demultiplexs}.r.l.u.fasta', demultiplexs=dfMultiChecked['demultiplex']),
        expand('../results/04_filter_samples/04_filtered/{demultiplexs}.c.r.l.u.fasta', demultiplexs=dfMultiChecked['demultiplex']),
        expand('../logs/04_filter_samples/01_dereplicated/{demultiplexs}.log', demultiplexs=dfMultiChecked['demultiplex']),
        expand('../logs/04_filter_samples/02_goodlength/{demultiplexs}.log', demultiplexs=dfMultiChecked['demultiplex']),
        expand('../logs/04_filter_samples/03_clean_pcrerr/{demultiplexs}.log', demultiplexs=dfMultiChecked['demultiplex']),
        expand('../logs/04_filter_samples/04_filtered/{demultiplexs}.log', demultiplexs=dfMultiChecked['demultiplex'])

include: "rules/dereplicate_samples.smk"
include: "rules/goodlength_samples.smk"
include: "rules/clean_pcrerr_samples.smk"
include: "rules/rm_internal_samples.smk"
