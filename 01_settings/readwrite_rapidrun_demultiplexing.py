#===============================================================================
#HEADER
#===============================================================================
__author__ = "Pierre-Edouard Guerin"
__credits__ = ["Pierre-Edouard Guerin", "Virginie Marques"]
__license__ = "MIT"
__version__ = "1.1.4"
__maintainer__ = "Pierre-Edouard Guerin"
__email__ = "pierre-edouard.guerin@cefe.cnrs.fr"
__status__ = "Production"
"""
Codes for scientific papers related to metabarcoding studies

AUTHORS
=======
* Pierre-Edouard Guerin   | pierre-edouard.guerin@cefe.cnrs.fr
* Virginie Marques        | virginie.marques@cefe.cnrs.fr
* CNRS/CEFE, CNRS/MARBEC  | Montpellier, France
* 2018-2020


DESCRIPTION
===========
This is a Snakefile using SNAKEMAKE workflow management system
From sample description files .dat files, config.yaml, rapidrun.tsv
it will return a demultiplexing.csv file. The output contains all required
wildcards en related information necessary to run the next workflow steps.
"""


###############################################################################
# MODULES
###############################################################################
import pandas
from Bio.Seq import Seq

###############################################################################
# FUNCTIONS
###############################################################################

## read a sample description .dat file and return a dataframe object
def read_dat(filedat):
    dfdat = pandas.read_csv(filedat, sep="\t", header=None)
    dfdat.columns=['experiment','plaque','barcode','primer5','primer3','F']
    return dfdat


###############################################################################
# GLOBAL VARIABLES
###############################################################################

## check format (CLASSIC or RAPIDRUN)
if config['format'] == "CLASSIC":
    print("CLASSIC data: one single marker for each run")    
    dfrClassic = pandas.DataFrame(columns=['plaque','run','sample','projet','marker'])
    for run in config['fichiers']['dat']:        
        thisRunDatfile=config['fichiers']['dat'][run]    
        thisDat=read_dat(thisRunDatfile)        
        for index, datRow in thisDat.iterrows():
            thisRow = {
                "plaque": datRow['plaque'],
                "run": run,
                "sample": datRow['plaque'],
                "projet": datRow['experiment'],
                "marker": run
            }
            dfrClassic = dfrClassic.append(thisRow, ignore_index=True)        
    print(dfrClassic)
    export_allsample = dfrClassic.to_csv (r'../results/01_settings/all_samples_classic.csv', index = None, header = False, sep = ';')
    rapidrunfile="../results/01_settings/all_samples_classic.csv"
else:
    print("RAPIDRUN data: many markers for many runs")
    #configfile: "01_infos/config.yaml"
    rapidrunfile = config['fichiers']['rapidrun']
    #rapidrunfile="01_infos/all_samples.tsv"




## read 'rapidrun' .tsv file
dfr =pandas.read_csv(rapidrunfile, sep=";")
dfr.columns = ['plaque', 'run','sample','projet','marker']
## remove blacklisted projets
blacklistedProjets = config['blacklist']['projet']
dfrp= dfr[dfr.run.isin(blacklistedProjets) == False]
## remove blacklisted runs
blacklistedRuns = config['blacklist']['run']
dfrm= dfrp[dfrp.run.isin(blacklistedRuns) == False]
## get list of `run`, `projet` and `marker` wildcards
uniqRuns=dfrm.run.unique()
uniqProjets=dfrm.projet.unique()
uniqMarkers=dfrm.marker.unique()


###############################################################################
# MAIN
###############################################################################

## list of dataframe of `marker` sample description .dat file
all_dat= {}
for marker in uniqMarkers:
    all_dat[marker]=read_dat(config['fichiers']['dat'][marker])
## init dataframe with all columns we want to output
dfMulti = pandas.DataFrame(columns=["demultiplex","projet", "marker","run", "plaque","sample","barcode5","barcode3","primer5","primer3","min_f","min_r","lenBarcode5","lenBarcode3"])
## fill the dataframe, each row belong to a `projet`/`marker`/`run`/`sample` wildcards combination
for run in uniqRuns:
    for marker in uniqMarkers:
        runMarkerDfrm=dfrm[(dfrm.run == run) & (dfrm.marker == marker)]
        for plaque in runMarkerDfrm.plaque:
            selectedRow=runMarkerDfrm[(runMarkerDfrm.plaque == plaque)]
            projet=selectedRow['projet'].values[0]
            sample=selectedRow['sample'].values[0]
            plaque_dat=all_dat[marker][(all_dat[marker].plaque == plaque)]
            barcode5=plaque_dat["barcode"].values[0]
            barcode3=str(Seq(barcode5).reverse_complement())
            primer5=plaque_dat["primer5"].values[0]
            #primer3=str(Seq(plaque_dat["primer3"].values[0]).reverse_complement())
            ## attention ici on utilise bien le .dat de ngsfilter
            primer3=plaque_dat["primer3"].values[0]
            lenBarcode5=len(barcode5)
            lenBarcode3=len(barcode3)
            min_f=int(int(len(primer5))*2/3)
            min_r=int(int(len(primer3))*2/3)
            sa_fastq=projet+"_"+marker+"/"+sample+".fastq"
            ru_fastq=run+".fastq"
            #print(marker, run, plaque, projet,sample)
            #print(ru_fastq)
            #print(sa_fastq)
            #print(barcode5, barcode3, primer5, primer3)
            #print(min_f, min_r, lenBarcode5,lenBarcode3)
            #print("========================")
            demultiplex=projet+"/"+marker+"/"+run+"/"+sample
            projmarkrun=projet+"/"+marker+"/"+run
            thisRow= {
            "demultiplex": demultiplex,
            "projmarkrun": projmarkrun,
            "projet": projet,
            "marker": marker,
            "run": run,
            "plaque": plaque,
            "sample": sample,
            "barcode5": barcode5,
            "barcode3": barcode3,
            "primer5": primer5,
            "primer3": primer3,
            "min_f": min_f,
            "min_r": min_r,
            "lenBarcode5": lenBarcode5,
            "lenBarcode3": lenBarcode3,
            }
            #print(thisRow)
            dfMulti = dfMulti.append( thisRow, ignore_index=True)
## write the demultiplexing .csv file
export_csv = dfMulti.to_csv (r'../results/01_settings/all_demultiplex.csv', index = None, header=True)
## print an overview of the dataframe we wrote
print (dfMulti)
