__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## The sequences can be sorted by decreasing order of count
rule sort_runs:
    input:
        '../results/05_assignment/04_formated/{run}.a.t.u.fasta'
    output:
        '../results/05_assignment/04_formated/{run}.s.a.t.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        '../logs/05_assignment/05_sort_runs/{run}.log'
    shell:
        '''obisort -k count -r {input} > {output} 2> {log}'''