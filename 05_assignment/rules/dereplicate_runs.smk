__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Dereplicate and merge samples together
rule dereplicate_runs:
    input:
        '../results/05_assignment/01_runs/{run}.fasta'
    output:
        '../results/05_assignment/02_dereplicated/{run}.uniq.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        '../logs/05_assignment/02_dereplicated/{run}.log'
    shell:
        '''obiuniq -m sample {input} > {output} 2> {log}'''