__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Assign each sequence to a taxon
rule assign_taxon:
    input:
        '../results/05_assignment/02_dereplicated/{run}.uniq.fasta'
    output:
        '../results/05_assignment/03_assigned/{run}.tag.u.fasta'
    threads:
        workflow.cores * 0.5
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:
        drun= lambda wildcards: dfpmr[dfpmr.projmarkrun == wildcards.run].to_dict('records')[0],
    log:
        '../logs/05_assignment/03_assign_taxon/{run}.log'
    shell:
        '''ecotag -d {params.drun[bdr]} -R {params.drun[fasta]} {input} > {output} 2> {log}'''