__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Generate a table final results
rule table_runs:
    input:
        '../results/05_assignment/04_formated/{run}.s.a.t.u.fasta'
    output:
        '../results/06_final_tables/{run}.csv'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:
        drun= lambda wildcards: dfpmr[dfpmr.projmarkrun == wildcards.run].to_dict('records')[0],
    log:
        '../logs/05_assignment/06_table_runs/{run}.log'
    shell:
        '''mkdir -p ../results/06_final_tables/{params.drun[projet]}/{params.drun[marker]}; obitab -o {input} > {output} 2> {log}'''