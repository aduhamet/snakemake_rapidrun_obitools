##########################################################################
## Codes for scientific papers related to metabarcoding studies
##
## Here, we reproduce the bioinformatics workflow used by SPYGEN 
## We improve its performance by adding parallelization and containerazition features.
## This workflow generates species environmental presence from raw eDNA data. 
## This workflow use the workflow management system SNAKEMAKE.
##
## AUTHORS
## =======
## * Pierre-Edouard Guerin   | pierre-edouard.guerin@cefe.cnrs.fr
## * Virginie Marques        | virginie.marques@etu.umontpellier.fr
## * CNRS/CEFE, CNRS/MARBEC  | Montpellier, France
## * 2018-2020
##
## USAGE
## =====
## CORES=8
## CONFIGFILE=config/config_tutorial_rapidrun.yaml
## bash main.sh $CORES $CONFIGFILE
##
## DESCRIPTION
## ===========
##
## 
##
##########################################################################
CORES=$1
CONFIGFILE=$2
##
#CORES=16
#CONFIGFILE="config/config_laperouse_marbec.yaml"
#CONFIGFILE="config/config_marbec_rapdirun_test.yaml"
#CONFIGFILE="config/config_marbec_rapdirun_test_sdx.yaml"
#CONFIGFILE="01_infos/config_test.yaml"
#CONFIGFILE="01_infos/config_laperouse_alsace.yaml"
#CONFIGFILE="config/config_tutorial_rapidrun.yaml"
#CONFIGFILE="config/config_tutorial_classic.yaml"

###############################################################################
## write demultiplex table
cd 01_settings
snakemake --configfile "../"$CONFIGFILE -s readwrite_rapidrun_demultiplexing.py --cores $CORES
wait
cd ..
## assemble
cd 02_assembly
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk --home $HOME" --latency-wait 20
snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-conda --latency-wait 20
wait
cd ..
## demultiplex
cd 03_demultiplex
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk --home $HOME" --latency-wait 20
snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-conda --latency-wait 20
wait
cd ..
## filter samples
cd 04_filter_samples
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk --home $HOME" --latency-wait 20
snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-conda --latency-wait 20
wait
cd ..
## concatenate samples into run
for projet in `ls results/04_filter_samples/04_filtered/`;
do 
 for marker in `ls results/04_filter_samples/04_filtered/${projet}/`;
 do
  for run in `ls results/04_filter_samples/04_filtered/${projet}/${marker}/`;
  do
  echo results/04_filter_samples/04_filtered/${projet}/${marker}/${run};
  mkdir -p results/05_assignment/01_runs/${projet}/${marker}/
  cat results/04_filter_samples/04_filtered/${projet}/${marker}/${run}/*.c.r.l.u.fasta > results/05_assignment/01_runs/${projet}/${marker}/${run}.fasta
  done
 done
done
## assignment
cd 05_assignment
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk --home $HOME" --latency-wait 20
snakemake --configfile "../"$CONFIGFILE -s Snakefile --cores $CORES --use-conda --latency-wait 20
wait
cd ..
echo "DONE"
############################################################################### 
## rename files
# bash rename.sh 06_assignment
## clean
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --delete-all-output --dry-run
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --delete-all-output --rerun-incomplete --cores 8
#nohup snakemake --configfile "../"$CONFIGFILE -s Snakefile -j $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk" --latency-wait 20 &
#snakemake --configfile "../"$CONFIGFILE -s Snakefile -j $CORES --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk" --latency-wait 20 --dry-run
